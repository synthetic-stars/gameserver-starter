package main

import (
	"github.com/spf13/viper"
	"log"
)

type Config struct {
	AuthKey        string `mapstructure:"key"`
	ContainerImage string `mapstructure:"image"`
	GameServerPort string `mapstructure:"server_port"`
	BindPortStart  int    `mapstructure:"bind_port_start"`
	BindPortRange  int    `mapstructure:"bind_port_range"`
	AssetsDir      string `mapstructure:"assets_dir"`
}

func InitConfig() *Config {
	config := new(Config)

	viper.SetDefault("key", "key")
	viper.SetDefault("image", "synthetictestserver")
	viper.SetDefault("server_port", "5874/udp")
	viper.SetDefault("bind_port_start", 60000)
	viper.SetDefault("bind_port_range", 100)
	viper.SetDefault("assets_dir", "/assets")

	viper.AutomaticEnv()

	err := viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	return config
}

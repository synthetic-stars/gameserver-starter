package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"go.uber.org/zap"
	"strconv"
	"time"
)

func startContainer(envs []string) (StartedContainer, error) {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		logger.Error("failed creating docker client",
			zap.Error(err),
		)
		return StartedContainer{}, errors.New("failed creating docker client")
	}

	imageName := config.ContainerImage
	out, err := cli.ImagePull(ctx, imageName, types.ImagePullOptions{})
	if err != nil {
		logger.Error("failed downloading image",
			zap.String("image", imageName),
			zap.Error(err),
		)
		return StartedContainer{}, errors.New("failed downloading image")
	}
	defer out.Close()

	bindPortStart := config.BindPortStart
	bindPortRange := config.BindPortRange

	var startedContainer StartedContainer
	for i := 0; i < bindPortRange; i++ {
		portConfig := nat.PortMap{
			nat.Port(config.GameServerPort): []nat.PortBinding{
				{HostPort: strconv.Itoa(bindPortStart + i)},
			},
		}

		resp, err := cli.ContainerCreate(ctx, &container.Config{
			Image:  imageName,
			Labels: map[string]string{"gameServer": "true"},
			Env:    envs,
		}, &container.HostConfig{
			PortBindings: portConfig,
			AutoRemove:   true,
			Binds:        []string{fmt.Sprintf("%s:/assets:ro", config.AssetsDir)},
		}, nil, nil, "")
		if err != nil {
			logger.Error("failed creating container",
				zap.Error(err),
			)
			return StartedContainer{}, errors.New("failed creating container")
		}
		if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err == nil {
			startedContainer = StartedContainer{
				Port: bindPortStart + i,
				Id:   resp.ID,
			}
			logger.Info("started game server container",
				zap.String("containerId", startedContainer.Id),
				zap.Int("containerPort", startedContainer.Port),
			)
			return startedContainer, nil
		}
	}

	logger.Warn("all available ports are in use")
	return StartedContainer{}, errors.New("all available ports are in use")
}

func stopContainer(id string) (string, error) {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		logger.Error("failed creating docker client",
			zap.Error(err),
		)
		return "", errors.New("failed creating docker client")
	}

	inspect, err := cli.ContainerInspect(ctx, id)
	if err != nil {
		logger.Info("failed inspecting container",
			zap.Error(err),
		)
		return "", errors.New("failed inspecting container")
	}

	if _, ok := inspect.Config.Labels["gameServer"]; ok {
		duration, _ := time.ParseDuration("5s")
		err := cli.ContainerStop(ctx, id, &duration)
		if err != nil {
			logger.Info("could not gracefully terminate container",
				zap.Duration("timeout", duration),
				zap.Error(err),
			)
			return "", err
		}

		logger.Info("stopped game server container",
			zap.String("containerId", id),
		)
		return id, nil
	}

	logger.Warn("tried to terminate a non game server container",
		zap.String("containerId", id),
	)
	return "", errors.New("not a game server container")
}

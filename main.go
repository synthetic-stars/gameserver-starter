package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

type StartedContainer struct {
	Port int
	Id   string
}

var config *Config
var logger *zap.Logger

func start(w http.ResponseWriter, r *http.Request) {
	envs := make([]string, 0)

	if err := json.NewDecoder(r.Body).Decode(&envs); err != nil {
		logger.Error("failed decoding json body",
			zap.Error(err),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	startedContainer, err := startContainer(envs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	response := make(map[string]string)
	response["port"] = strconv.Itoa(startedContainer.Port)
	response["containerId"] = startedContainer.Id
	jsonResp, _ := json.Marshal(response)
	if _, err := w.Write(jsonResp); err != nil {
		panic(err)
	}
}

func stop(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	stoppedContainer, err := stopContainer(params["containerId"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Write([]byte(stoppedContainer))
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func main() {
	config = InitConfig()
	logger, _ = zap.NewProduction()
	defer logger.Sync()

	router := mux.NewRouter()
	router.HandleFunc("/start", start).Queries("key", config.AuthKey).Methods("POST")
	router.HandleFunc("/stop/{containerId}", stop).Queries("key", config.AuthKey)
	router.HandleFunc("/healthcheck", healthcheck)
	http.ListenAndServe(":80", router)
}
